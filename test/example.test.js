const expect = require('chai').expect;
const { divide } = require('../src/mylib');
const mylib = require('../src/mylib');

describe("Our first unit tests", () => {
    before(() => {
        // Initialization
        // create objects... etc
        console.log("Initialising tests");
    });
    it("Should return 3 when using sum function with a=1, b=2", () => {
        // Tests 
        expect(mylib.add(1,2)).equal(3, "1 + 2 is not 3, for some reason?");
    });
    it("Should return 2 when using subtract function with a=5, b=3", () => {
        expect(mylib.subtract(5,3)).equal(2, "5 - 3 is not 2, for some reason?");
    });
    it("Should return 4 when using divide function with dividend=8, divisor=2", () => {
        expect(mylib.divide(8,2)).equal(4, "8 / 2 is not 4, for some reason?");
    });
    it("Should return 6 when using multiply function with a=2, b=3", () => {
        expect(mylib.multiply(2,3)).equal(6, "2 * 3 is not 6, for some reason?");
    });
    it("Should throw error when trying to divide by zero", () => {
        expect(function() {
          divide(8,0);  
        }).to.throw(Error);
    });
    after("", () => {
        // Cleanup
        // For example: shut the Express server.
        console.log("Testing completed!");
    });
});